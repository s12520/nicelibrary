using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NiceLibrary.Model;
using NiceLibrary.data;

namespace NiceLibrary.Controllers
{
    public class BorrowersController : Controller
    {
        private readonly LibraryContext _context;

        public BorrowersController(LibraryContext context)
        {
            _context = context;    
        }

        // GET: Borrowers
        public async Task<IActionResult> Index(
                string sortOrder,
                string currentFilter,
                string searchString,
                int? page)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DateSortParm"] = sortOrder == "Date" ? "date_desc" : "Date";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;
            var borrowers = from s in _context.Borrowers
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                borrowers = borrowers.Where(s => s.LastName.Contains(searchString)
                                       || s.FirstName.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                            borrowers = borrowers.OrderByDescending(s => s.LastName);
                    break;
                case "Date":
                            borrowers = borrowers.OrderBy(s => s.EnrollmentDate);
                    break;
                case "date_desc":
                            borrowers = borrowers.OrderByDescending(s => s.EnrollmentDate);
                    break;
                default:
                            borrowers = borrowers.OrderBy(s => s.LastName);
                    break;
            }
            int pageSize = 6;
            return View(await PaginatedList<Borrower>.CreateAsync(borrowers.AsNoTracking(), page ?? 1, pageSize));
        }

        // GET: Borrowers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var borrower = await _context.Borrowers
                .Include(s => s.Borrowings)
                .ThenInclude(e => e.Book)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);

          
            if (borrower == null)
            {
                return NotFound();
            }

            return View(borrower);
        }

        // GET: Borrowers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Borrowers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,LastName,FirstName,EnrollmentDate")] Borrower borrower)
        {
            try
            {
                if (ModelState.IsValid)
            {
                _context.Add(borrower);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.
                ModelState.AddModelError("", "Unable to save changes. " +
                    "Try again, and if the problem persists " +
                    "see your system administrator.");
            }

            return View(borrower);
        }

        // GET: Borrowers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

           var borrower = await _context.Borrowers
                .Include(s => s.Borrowings)
                .ThenInclude(e => e.Book)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (borrower == null)
            {
                return NotFound();
            }
            return View(borrower);
        }

        // POST: Borrowers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,LastName,FirstName,EnrollmentDate")] Borrower borrower, string[] selectedBooks)
        {
            if (id != borrower.ID)
            {
                return NotFound();
            }

            var borroweToUpdate = await _context.Borrowers
                .Include(i => i.Borrowings)

                    .ThenInclude(i => i.Book)
                .SingleOrDefaultAsync(m => m.ID == id);


            if (await TryUpdateModelAsync<Borrower>(
               borroweToUpdate,
               "",
               i => i.FirstName, i => i.LastName, i => i.EnrollmentDate, i => i.Borrowings))
            {
           //     if (String.IsNullOrWhiteSpace(borroweToUpdate.OfficeAssignment?.Location))
           //     {
           //         borroweToUpdate.OfficeAssignment = null;
           //     }
           //     UpdateInstructorCourses(selectedCourses, instructorToUpdate);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
                return RedirectToAction("Index");
            }





            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(borrower);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BorrowerExists(borrower.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(borrower);
        }


        /* Edit backed up
        public async Task<IActionResult> Edit(int id, [Bind("ID,LastName,FirstName,EnrollmentDate")] Borrower borrower)
        {
            if (id != borrower.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(borrower);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BorrowerExists(borrower.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(borrower);
        } */










        // GET: Borrowers/Delete/5
        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var borrower = await _context.Borrowers
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (borrower == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                    "Delete failed. Try again, and if the problem persists " +
                    "see your system administrator.";
            }

            return View(borrower);
        }

        // POST: Borrowers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var borrower = await _context.Borrowers
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.ID == id);
            if (borrower == null)
            {
                return RedirectToAction("Index");
            }

            try
            {
                _context.Borrowers.Remove(borrower);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.)
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
        }

        private bool BorrowerExists(int id)
        {
            return _context.Borrowers.Any(e => e.ID == id);
        }
    }
}
