﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NiceLibrary.Model
{
    public class Borrowing
    {
        public int BorrowingID { get; set; }
        public int BookID { get; set; }
        public int BorrowerID { get; set; }
        public bool Active { get; set; }


        public Book Book { get; set; }
        public Borrower Borrower { get; set; }
    }
}
