﻿using System;
using System.Linq;
using NiceLibrary.data;
using NiceLibrary.Model;

namespace NiceLibrary.Data
{
    public static class DbInitializer
    {
        public static void Initialize(LibraryContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Borrowers.Any())
            {
                return;   // DB has been seeded
            }

            var borrowers = new Borrower[]
            {
            new Borrower{FirstName="Carson",LastName="Alexander",EnrollmentDate=DateTime.Parse("2005-09-01")},
            new Borrower{FirstName="Meredith",LastName="Alonso",EnrollmentDate=DateTime.Parse("2002-09-01")},
            new Borrower{FirstName="Arturo",LastName="Anand",EnrollmentDate=DateTime.Parse("2003-09-01")},
            new Borrower{FirstName="Gytis",LastName="Barzdukas",EnrollmentDate=DateTime.Parse("2002-09-01")},
            new Borrower{FirstName="Yan",LastName="Li",EnrollmentDate=DateTime.Parse("2002-09-01")},
            new Borrower{FirstName="Peggy",LastName="Justice",EnrollmentDate=DateTime.Parse("2001-09-01")},
            new Borrower{FirstName="Laura",LastName="Norman",EnrollmentDate=DateTime.Parse("2003-09-01")},
            new Borrower{FirstName="Nino",LastName="Olivetto",EnrollmentDate=DateTime.Parse("2005-09-01")}
            };
            foreach (Borrower s in borrowers)
            {
                context.Borrowers.Add(s);
            }
            context.SaveChanges();

            var books = new Book[]
            {
            new Book{BookID=1050,Title="Najpiękniejsze mosty świata",Author="Irteński Tadeusz",Genre="albumy",Borrowed=false},
            new Book{BookID=4022,Title="Odczuwanie architektury",Author="Rassmusen Steen Eiler",Genre="sztuka",Borrowed=false},
            new Book{BookID=4041,Title="Warszawa lata 60",Author="Opracowanie zbiorow",Genre="sztuka",Borrowed=false},
            new Book{BookID=1045,Title="Kobiety, które kochają za bardzo",Author="Norwood Robin",Genre="psychologia",Borrowed=false},
            new Book{BookID=3141,Title="Potęga podświadomości",Author="Murphy Joseph",Genre="psychologia",Borrowed=false},
            new Book{BookID=2021,Title="Zbiór karny 2015",Author="Opracowanie zbiorowe",Genre="prawo",Borrowed=false},
            new Book{BookID=2042,Title="Zbiór cywilny 2015",Author="Opracowanie zbiorowe",Genre="prawo",Borrowed=false}
            };
            foreach (Book c in books)
            {
                context.Books.Add(c);
            }
            context.SaveChanges();

            var borrowings = new Borrowing[]
            {
            new Borrowing{BorrowerID=1,BookID=1050,Active =true},
            new Borrowing{BorrowerID=1,BookID=4022,Active =true},
            new Borrowing{BorrowerID=1,BookID=4041,Active =true},
            new Borrowing{BorrowerID=2,BookID=1045,Active =false},
            new Borrowing{BorrowerID=2,BookID=3141,Active =true},
            new Borrowing{BorrowerID=2,BookID=2021,Active =true},
            new Borrowing{BorrowerID=3,BookID=1050,Active =true},
            new Borrowing{BorrowerID=4,BookID=1050,Active =true},
            new Borrowing{BorrowerID=4,BookID=4022,Active =false},
            new Borrowing{BorrowerID=5,BookID=4041,Active =true},
            new Borrowing{BorrowerID=6,BookID=1045,Active =true},
            new Borrowing{BorrowerID=7,BookID=3141,Active =true},
            };
            foreach (Borrowing e in borrowings)
            {
                context.Borrowings.Add(e);
            }
            context.SaveChanges();
        }
    }
}