﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NiceLibrary.Model;
namespace NiceLibrary.data
{
    public class LibraryContext : DbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {
        }
        public DbSet<Book> Books { get; set; } //Course
        public DbSet<Borrowing> Borrowings { get; set; } //enrollments
        public DbSet<Borrower> Borrowers { get; set; } //student

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().ToTable("Book");
            modelBuilder.Entity<Borrowing>().ToTable("Borrowing");
            modelBuilder.Entity<Borrower>().ToTable("Borrower");
        }
    }
}
